package com.jorgejbarra.filemanager.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.jorgejbarra.filemanager.domain.FileRepository
import org.springframework.core.io.InputStreamResource
import org.springframework.core.io.Resource
import org.springframework.http.ContentDisposition
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController


@RestController
class DownloadController(private var fileRepository: FileRepository) {

    private val objectMapper = ObjectMapper()

    @GetMapping(
        "/fileAsLink/{id}",
        produces = ["application/octet-stream"]
    )
    fun downloadAsTemporalUrl(@PathVariable("id") resourceId: String): ResponseEntity<String> {
        val downloadResource = fileRepository.downloadAsLink(resourceId)
        return ResponseEntity(objectMapper.writeValueAsString(downloadResource), HttpStatus.OK)
    }

    @GetMapping(
        "/fileAsFile/{id}",
        produces = ["application/octet-stream"]
    )
    fun downloadFile(@PathVariable("id") resourceId: String): ResponseEntity<Resource> {
        val downloadResource = fileRepository.downloadAsFile(resourceId)
        val inputStreamResource = InputStreamResource(downloadResource.inputStream)
        val headers = HttpHeaders().apply {
            contentType = MediaType.APPLICATION_OCTET_STREAM
            contentDisposition = ContentDisposition.parse("attachment; filename=" + downloadResource.fileName)
        }

        return ResponseEntity(inputStreamResource, headers, HttpStatus.OK)
    }
}

