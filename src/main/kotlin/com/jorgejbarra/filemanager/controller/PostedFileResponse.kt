package com.jorgejbarra.filemanager.controller

import com.fasterxml.jackson.annotation.JsonProperty

data class PostedFileResponse(
    @JsonProperty("resourceId") val resourceId: String
)
