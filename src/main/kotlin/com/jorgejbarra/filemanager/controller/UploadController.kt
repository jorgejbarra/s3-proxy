package com.jorgejbarra.filemanager.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.jorgejbarra.filemanager.application.UploadService
import com.jorgejbarra.filemanager.domain.UploaderResult
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

@RestController
class UploadController {

    @Autowired
    lateinit var uploadService: UploadService

    private val objectMapper = ObjectMapper()

    @PostMapping(
        "/file",
        consumes = ["multipart/form-data"],
        produces = ["application/json; charset=UTF-8"]
    )
    fun upload(@RequestParam("file") file: MultipartFile): String =
        objectMapper.writeValueAsString(uploadService.upload(file))

    private fun UploaderResult.toPostedFileResponse(): PostedFileResponse {
        return PostedFileResponse(this.resourceId)
    }
}
