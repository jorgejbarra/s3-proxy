package com.jorgejbarra.filemanager.infrastructure.framework.controlleradvice

import com.amazonaws.AmazonClientException
import com.amazonaws.services.s3.model.AmazonS3Exception
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class AwsControllerAdvice {

    @ResponseBody
    @ExceptionHandler(AmazonClientException::class)
    fun handleAmazonClientException(amazonClientException: AmazonS3Exception): ResponseEntity<String> =
        when (amazonClientException.statusCode) {
            403, 404 -> ResponseEntity(HttpStatus.NOT_FOUND)
            else -> ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
}
