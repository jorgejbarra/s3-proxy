package com.jorgejbarra.filemanager.infrastructure.framework.configuration

import com.jorgejbarra.filemanager.application.UUIDGenerator
import com.jorgejbarra.filemanager.application.UploadService
import com.jorgejbarra.filemanager.domain.FileRepository
import com.jorgejbarra.filemanager.infrastructure.repository.S3FileRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class FileManagerConfiguration {

    @Bean
    fun getFileRepository() = S3FileRepository()

    @Bean
    fun getUploadService(fileRepository: FileRepository, uuidGenerator: UUIDGenerator): UploadService =
        UploadService(fileRepository, uuidGenerator)

    @Bean
    fun getUUIDGenerator() = UUIDGenerator()
}