package com.jorgejbarra.filemanager.infrastructure.repository

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.ObjectMetadata
import com.jorgejbarra.filemanager.application.UUIDGenerator
import com.jorgejbarra.filemanager.domain.DownloadResource
import com.jorgejbarra.filemanager.domain.FileRepository
import com.jorgejbarra.filemanager.domain.LinkedResource
import com.jorgejbarra.filemanager.domain.UploaderResult
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.web.multipart.MultipartFile
import java.util.Date

@EnableConfigurationProperties
class S3FileRepository : FileRepository {

    @Value("\${amazon.s3.accessKey}")
    private lateinit var accessKey: String
    @Value("\${amazon.s3.secretKey}")
    private lateinit var secretKey: String
    @Value("\${amazon.region.static}")
    private lateinit var region: String
    @Value("\${amazon.s3.bucket}")
    private lateinit var bucket: String

    private val awsStaticCredentialsProvider by lazy {
        AWSStaticCredentialsProvider(BasicAWSCredentials(accessKey, secretKey))
    }

    private val s3client by lazy {
        AmazonS3ClientBuilder
            .standard()
            .withCredentials(awsStaticCredentialsProvider)
            .withRegion(region)
            .build()
    }

    override fun upload(file: MultipartFile, uuidGenerator: UUIDGenerator): UploaderResult {
        val objectMetadata = ObjectMetadata().apply {
            contentType = file.contentType
            contentLength = file.size
            userMetadata.put("fileName", file.originalFilename)
        }

        val resourceId = uuidGenerator.getNew().toString()
        s3client.putObject(bucket, resourceId, file.inputStream, objectMetadata)

        return UploaderResult(resourceId)
    }

    override fun downloadAsLink(resourceId: String): LinkedResource {
        val expiration = Date().apply { time += 1000L * 60L * 60L }

        val presignedUrl = s3client.generatePresignedUrl(bucket, resourceId, expiration)

        return LinkedResource(presignedUrl)
    }

    override fun downloadAsFile(fileName: String): DownloadResource {
        val s3object = s3client.getObject(bucket, fileName)

        val originalFilename = s3object.objectMetadata.userMetadata.getValue("fileName")
        return DownloadResource(originalFilename, s3object.objectContent)
    }
}
