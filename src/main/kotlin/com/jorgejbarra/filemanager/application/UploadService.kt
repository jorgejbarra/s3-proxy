package com.jorgejbarra.filemanager.application

import com.jorgejbarra.filemanager.controller.PostedFileResponse
import com.jorgejbarra.filemanager.domain.FileRepository
import com.jorgejbarra.filemanager.domain.UploaderResult
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class UploadService(
    private val fileRepository: FileRepository,
    private val uuidGenerator: UUIDGenerator
) {

    fun upload(file: MultipartFile): PostedFileResponse =
        fileRepository.upload(file, uuidGenerator).toPostedFileResponse()

    private fun UploaderResult.toPostedFileResponse(): PostedFileResponse {
        return PostedFileResponse(this.resourceId)
    }
}
