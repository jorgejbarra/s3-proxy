package com.jorgejbarra.filemanager.application

import java.util.UUID

class UUIDGenerator {
    fun getNew(): UUID = UUID.randomUUID()
}
