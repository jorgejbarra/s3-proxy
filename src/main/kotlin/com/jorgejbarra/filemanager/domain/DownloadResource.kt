package com.jorgejbarra.filemanager.domain

import java.io.InputStream

class DownloadResource(val fileName: String, val inputStream: InputStream)
