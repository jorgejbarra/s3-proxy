package com.jorgejbarra.filemanager.domain

import com.jorgejbarra.filemanager.application.UUIDGenerator
import org.springframework.web.multipart.MultipartFile

interface FileRepository {
    fun upload(file: MultipartFile, uuidGenerator: UUIDGenerator): UploaderResult
    fun downloadAsLink(resourceId: String): LinkedResource
    fun downloadAsFile(fileName: String): DownloadResource
}
