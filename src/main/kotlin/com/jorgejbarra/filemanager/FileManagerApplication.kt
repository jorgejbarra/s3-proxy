package com.jorgejbarra.filemanager

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FileManagerApplication

fun main(args: Array<String>) {
    runApplication<FileManagerApplication>(*args)
}
